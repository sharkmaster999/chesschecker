﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ChessChecker
{
    public partial class Form1 : Form
    {
        int a = 0, b = 0;
        bool isClick = false;
        int click_xcoor = 0;
        int click_ycoor = 0;

        const int p1_king = 10;
        const int p1_queen = 9;
        const int p1_bishop = 8;
        const int p1_knight = 7;
        const int p1_rook = 6;
        const int p1_pawn = 1;

        const int p2_king = 100;
        const int p2_queen = 99;
        const int p2_bishop = 98;
        const int p2_knight = 97;
        const int p2_rook = 96;
        const int p2_pawn = 91;

        List<int[]> suggestedMoves = new List<int[]>();
        List<int[]> suggestedCapture = new List<int[]>();

        int[,] board = new int[,] { { 6,  7,  8,  9,  10,  8,  7,  6 }, 
                                    { 1,  1,  1,  1,  1,   1,  1,  1 },
                                    { 0,  0,  0,  0,  0,   0,  0,  0 }, 
                                    { 0,  0,  0,  0,  0,   0,  0,  0 }, 
                                    { 0,  0,  0,  0,  0,   0,  0,  0 }, 
                                    { 0,  0,  0,  0,  0,   0,  0,  0 }, 
                                    { 91, 91, 91, 91, 91,  91, 91, 91 },  
                                    { 96, 97, 98, 99, 100, 98, 97, 96 }};


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            RenderImage();
        }

        private void RenderImage() {
            Bitmap gPlane = new Bitmap(61 * board.GetLength(1), 61 * board.GetLength(0));
            Graphics g = Graphics.FromImage(gPlane);

            Pen pen = new Pen(Color.Black, 1);
            pen.Alignment = PenAlignment.Inset; 

            for (int i = 0; i < 8; i++)
            {
                b = 0;
                for (int j = 0; j < 8; j++)
                {
                    if (i % 2 == 0)
                    {
                        if (j % 2 == 1)
                        {

                            g.FillRectangle(Brushes.Green, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                        }
                        else
                        {
                            g.FillRectangle(Brushes.Silver, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                        }
                    }
                    else
                    {
                        if (j % 2 == 1)
                        {
                            g.FillRectangle(Brushes.Silver, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                        }
                        else
                        {
                            g.FillRectangle(Brushes.Green, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                        }
                    }

                    if (isClick) {
                        if (i == click_xcoor && j == click_ycoor) {
                            g.FillRectangle(Brushes.Yellow, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                        }
                    }

                    if (!(suggestedMoves == null))
                    {
                        foreach (var item in suggestedMoves)
                        {
                            if (i == item[0] && j == item[1])
                            {
                                g.FillRectangle(Brushes.YellowGreen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                                g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            }
                        }
                    }

                    if (!(suggestedCapture == null))
                    {
                        foreach (var item in suggestedCapture)
                        {
                            if (i == item[0] && j == item[1])
                            {
                                g.FillRectangle(Brushes.Red, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                                g.DrawRectangle(pen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            }
                        }
                    }

                    switch (board[i, j])
                    {
                        case p1_rook:
                            g.DrawImage(Properties.Resources.b_rook, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p1_knight:
                            g.DrawImage(Properties.Resources.b_knight, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p1_bishop:
                            g.DrawImage(Properties.Resources.b_bishop, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p1_queen:
                            g.DrawImage(Properties.Resources.b_queen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p1_king:
                            g.DrawImage(Properties.Resources.b_king, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p1_pawn:
                            g.DrawImage(Properties.Resources.b_pawn, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;


                        case p2_rook:
                            g.DrawImage(Properties.Resources.w_rook, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p2_knight:
                            g.DrawImage(Properties.Resources.w_knight, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p2_bishop:
                            g.DrawImage(Properties.Resources.w_bishop, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p2_queen:
                            g.DrawImage(Properties.Resources.w_queen, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p2_king:
                            g.DrawImage(Properties.Resources.w_king, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        case p2_pawn:
                            g.DrawImage(Properties.Resources.w_pawn, new Rectangle(new Point(b * 60, a * 60), new Size(60, 60)));
                            break;
                        default:
                            break;
                    }
                    b++;
                }
                a++;

            }

            a = 0; b = 0;
            pictureBox1.Image = gPlane;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            

            if (board[coord(y), coord(x)] == p1_pawn)
                pawnMoves(coord(y), coord(x), 'b');
            else if (board[coord(y), coord(x)] == p2_pawn)
                pawnMoves(coord(y), coord(x), 'w');
            else if (board[coord(y), coord(x)] == p1_rook)
                rookMoves(coord(y), coord(x), 'b');
            else if (board[coord(y), coord(x)] == p2_rook)
                rookMoves(coord(y), coord(x), 'w');
            else if (board[coord(y), coord(x)] == p1_bishop)
                bishopMoves(coord(y), coord(x), 'b');
            else if (board[coord(y), coord(x)] == p2_bishop)
                bishopMoves(coord(y), coord(x), 'w');
            else if (board[coord(y), coord(x)] == p1_queen)
            {
                rookMoves(coord(y), coord(x), 'b');
                bishopMoves(coord(y), coord(x), 'b');
            }
            else if (board[coord(y), coord(x)] == p2_queen)
            {
                rookMoves(coord(y), coord(x), 'w');
                bishopMoves(coord(y), coord(x), 'w');
            }
            else if (board[coord(y), coord(x)] == p1_knight)
                knightMoves(coord(y), coord(x), 'b');
            else if (board[coord(y), coord(x)] == p2_knight)
                knightMoves(coord(y), coord(x), 'w');
            else if (board[coord(y), coord(x)] == p1_king)
                kingMoves(coord(y), coord(x), 'b');
            else if (board[coord(y), coord(x)] == p2_king)
                kingMoves(coord(y), coord(x), 'w');


            if (isClick)
            {
                bool flag = false;
                bool flag1 = false;
                foreach (var item in suggestedMoves)
                {

                    if (item[0] == coord(y) && item[1] == coord(x))
                    {
                        flag = true;
                        break;
                    }
                }

                foreach (var item in suggestedCapture)
                {
                    if (item[0] == coord(y) && item[1] == coord(x))
                    {
                        flag1 = true;
                        break;
                    }
                }

                if (board[coord(y), coord(x)] == 0)
                {
                    if (flag || flag1)
                    {
                        board[coord(y), coord(x)] = board[click_xcoor, click_ycoor];
                        board[click_xcoor, click_ycoor] = 0;
                        isClick = false;
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                        click_xcoor = 0;
                        click_ycoor = 0;
                    }
                    else
                    {
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                    }
                }
                else if (board[click_xcoor, click_ycoor] < 10 && board[coord(y), coord(x)] > 10 && !(board[click_xcoor, click_ycoor] == 0))
                {
                    if (flag || flag1)
                    {
                        board[coord(y), coord(x)] = board[click_xcoor, click_ycoor];
                        board[click_xcoor, click_ycoor] = 0;
                        isClick = false;
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                        click_xcoor = 0;
                        click_ycoor = 0;
                    }
                    else
                    {
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                    }
                }
                else if (board[click_xcoor, click_ycoor] > 10 && board[coord(y), coord(x)] < 10 && !(board[coord(y), coord(x)] == 0))
                {
                    if (flag || flag1)
                    {
                        board[coord(y), coord(x)] = board[click_xcoor, click_ycoor];
                        board[click_xcoor, click_ycoor] = 0;
                        isClick = false;
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                        click_xcoor = 0;
                        click_ycoor = 0;
                    }
                    else
                    {
                        suggestedCapture.Clear();
                        suggestedMoves.Clear();
                    }
                }
                else if (board[click_xcoor, click_ycoor] > 10 && board[coord(y), coord(x)] > 10 && !(board[coord(y), coord(x)] == 0))
                {
                    isClick = false;
                    suggestedCapture.Clear();
                    suggestedMoves.Clear();
                    click_xcoor = 0;
                    click_ycoor = 0;
                }
                else if (board[click_xcoor, click_ycoor] < 11 && board[coord(y), coord(x)] < 11 && !(board[coord(y), coord(x)] == 0))
                {
                    isClick = false;
                    suggestedCapture.Clear();
                    suggestedMoves.Clear();
                    click_xcoor = 0;
                    click_ycoor = 0;
                }
            }
            else { isClick = true; }


            label2.Text = String.Format("({0}, {1})", coord(y), coord(x));
            label3.Text = String.Format("{0}", board[coord(y), coord(x)]);

            if (isClick) {
                click_xcoor = coord(y);
                click_ycoor = coord(x);
            }

            RenderImage();
        }

        public int coord(int x) {
            if (x < 60)
                return 0;
            else if (x < 120)
                return 1;
            else if (x < 180)
                return 2;
            else if (x < 240)
                return 3;
            else if (x < 300)
                return 4;
            else if (x < 360)
                return 5;
            else if (x < 420)
                return 6;
            else
                return 7;
        }

        public void kingMoves(int xcoor, int ycoor, char c) {
            if (c == 'b') {
                if ((xcoor + 1) < 8)
                {
                    if (board[xcoor + 1, ycoor] > 10)
                        suggestedCapture.Add(new int[] { xcoor + 1, ycoor });

                    if (board[xcoor + 1, ycoor] == 0)
                        suggestedMoves.Add(new int[] { xcoor + 1, ycoor });

                    if ((ycoor - 1) >= 0)
                    {
                        if (board[xcoor + 1, ycoor - 1] > 10)
                            suggestedCapture.Add(new int[] { xcoor + 1, ycoor - 1 });

                        if (board[xcoor + 1, ycoor - 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor + 1, ycoor - 1 });
                    }

                    if ((ycoor + 1) < 8)
                    {
                        if (board[xcoor + 1, ycoor + 1] > 10)
                            suggestedCapture.Add(new int[] { xcoor + 1, ycoor + 1 });

                        if (board[xcoor + 1, ycoor + 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor + 1, ycoor + 1 });
                    }
                }

                if ((xcoor - 1) >= 0)
                {
                    if (board[xcoor - 1, ycoor] > 10)
                        suggestedCapture.Add(new int[] { xcoor - 1, ycoor });

                    if (board[xcoor - 1, ycoor] == 0)
                        suggestedMoves.Add(new int[] { xcoor - 1, ycoor });

                    if ((ycoor - 1) >= 0)
                    {
                        if (board[xcoor - 1, ycoor - 1] > 10)
                            suggestedCapture.Add(new int[] { xcoor - 1, ycoor - 1 });

                        if (board[xcoor - 1, ycoor - 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor - 1, ycoor - 1 });
                    }

                    if ((ycoor + 1) < 8)
                    {
                        if (board[xcoor - 1, ycoor + 1] > 10)
                            suggestedCapture.Add(new int[] { xcoor - 1, ycoor + 1 });

                        if (board[xcoor - 1, ycoor + 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor - 1, ycoor + 1 });
                    }
                }

                if ((ycoor - 1) >= 0)
                {
                    if (board[xcoor, ycoor - 1] > 10)
                        suggestedCapture.Add(new int[] { xcoor, ycoor - 1 });

                    if (board[xcoor, ycoor - 1] == 0)
                        suggestedMoves.Add(new int[] { xcoor, ycoor - 1 });
                } 
                
                if ((ycoor + 1) < 8)
                {
                    if (board[xcoor, ycoor + 1] > 10)
                        suggestedCapture.Add(new int[] { xcoor, ycoor + 1 });

                    if (board[xcoor, ycoor + 1] == 0)
                        suggestedMoves.Add(new int[] { xcoor, ycoor + 1 });
                }
            }

            else if (c == 'w')
            {
                if ((xcoor + 1) < 8)
                {
                    if (board[xcoor + 1, ycoor] <= 10 && board[xcoor + 1, ycoor] > 0)
                        suggestedCapture.Add(new int[] { xcoor + 1, ycoor });

                    if (board[xcoor + 1, ycoor] == 0)
                        suggestedMoves.Add(new int[] { xcoor + 1, ycoor });

                    if ((ycoor - 1) >= 0)
                    {
                        if (board[xcoor + 1, ycoor - 1] <= 10 && board[xcoor + 1, ycoor - 1] > 0)
                            suggestedCapture.Add(new int[] { xcoor + 1, ycoor - 1 });

                        if (board[xcoor + 1, ycoor - 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor + 1, ycoor - 1 });
                    }

                    if ((ycoor + 1) < 8)
                    {
                        if (board[xcoor + 1, ycoor + 1] <= 10 && board[xcoor + 1, ycoor + 1] > 0)
                            suggestedCapture.Add(new int[] { xcoor + 1, ycoor + 1 });

                        if (board[xcoor + 1, ycoor + 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor + 1, ycoor + 1 });
                    }
                }

                if ((xcoor - 1) >= 0)
                {
                    if (board[xcoor - 1, ycoor] <= 10 && board[xcoor - 1, ycoor] > 0)
                        suggestedCapture.Add(new int[] { xcoor - 1, ycoor });

                    if (board[xcoor - 1, ycoor] == 0)
                        suggestedMoves.Add(new int[] { xcoor - 1, ycoor });

                    if ((ycoor - 1) >= 0)
                    {
                        if (board[xcoor - 1, ycoor - 1] <= 10 && board[xcoor - 1, ycoor - 1] > 0)
                            suggestedCapture.Add(new int[] { xcoor - 1, ycoor - 1 });

                        if (board[xcoor - 1, ycoor - 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor - 1, ycoor - 1 });
                    }

                    if ((ycoor + 1) < 8)
                    {
                        if (board[xcoor - 1, ycoor + 1] <= 10 && board[xcoor - 1, ycoor + 1] > 0)
                            suggestedCapture.Add(new int[] { xcoor - 1, ycoor + 1 });

                        if (board[xcoor - 1, ycoor + 1] == 0)
                            suggestedMoves.Add(new int[] { xcoor - 1, ycoor + 1 });
                    }
                }

                if ((ycoor - 1) >= 0)
                {
                    if (board[xcoor, ycoor - 1] <= 10 && board[xcoor, ycoor - 1] > 0)
                        suggestedCapture.Add(new int[] { xcoor, ycoor - 1 });

                    if (board[xcoor, ycoor - 1] == 0)
                        suggestedMoves.Add(new int[] { xcoor, ycoor - 1 });
                }

                if ((ycoor + 1) < 8)
                {
                    if (board[xcoor, ycoor + 1] <= 10 && board[xcoor, ycoor - 1] > 0)
                        suggestedCapture.Add(new int[] { xcoor, ycoor + 1 });

                    if (board[xcoor, ycoor + 1] == 0)
                        suggestedMoves.Add(new int[] { xcoor, ycoor + 1 });
                }
            }
        }

        public void knightMoves(int xcoor, int ycoor, char c) {
            if (c == 'b')
            {
                int j = 2;
                for (int i = 1; i < 3; i++) {
                    if ((((ycoor + j) < 8) && ((ycoor - j) >= 0)))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor + j] > 10)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor + j });

                            if (board[xcoor + i, ycoor + j] == 0)
                            {
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor + j });
                            }

                            if (board[xcoor + i, ycoor - j] > 10)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor - j });

                            if (board[xcoor + i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor - j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor + j] > 10)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor - j] > 10)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor - j });

                            if (board[xcoor - i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor - j });
                        }
                    }
                    else if (((ycoor + j) < 8))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor + j] > 10)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor + j });

                            if (board[xcoor + i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor + j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor + j] > 10)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor + j });
                        }
                    }
                    else if (((ycoor - j) >= 0))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor - j] > 10)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor - j });

                            if (board[xcoor + i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor - j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor - j] > 10)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor - j });

                            if (board[xcoor - i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor - j });
                        }
                    }

                    j--;
                }
            }
            else if (c == 'w')
            {
                int j = 2;
                for (int i = 1; i < 3; i++)
                {
                    if ((((ycoor + j) < 8) && ((ycoor - j) >= 0)))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor + j] <= 10 && board[xcoor + i, ycoor + j] > 0)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor + j });

                            if (board[xcoor + i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor + j });

                            if (board[xcoor + i, ycoor - j] <= 10 && board[xcoor + i, ycoor - j] > 0)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor - j });

                            if (board[xcoor + i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor - j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor + j] <= 10 && board[xcoor - i, ycoor + j] > 0)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor - j] <= 10 && board[xcoor - i, ycoor - j] > 0)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor - j });

                            if (board[xcoor - i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor - j });
                        }
                    }
                    else if (((ycoor + j) < 8))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor + j] <= 10 && board[xcoor + i, ycoor + j] > 0)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor + j });

                            if (board[xcoor + i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor + j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor + j] <= 10 && board[xcoor - i, ycoor + j] > 0)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor + j });

                            if (board[xcoor - i, ycoor + j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor + j });
                        }
                    }
                    else if (((ycoor - j) >= 0))
                    {
                        if (((xcoor + i) < 8))
                        {
                            if (board[xcoor + i, ycoor - j] <= 10 && board[xcoor + i, ycoor - j] > 0)
                                suggestedCapture.Add(new int[] { xcoor + i, ycoor - j });

                            if (board[xcoor + i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor + i, ycoor - j });
                        }

                        if (((xcoor - i) >= 0))
                        {
                            if (board[xcoor - i, ycoor - j] <= 10 && board[xcoor - i, ycoor - j] > 0)
                                suggestedCapture.Add(new int[] { xcoor - i, ycoor - j });

                            if (board[xcoor - i, ycoor - j] == 0)
                                suggestedMoves.Add(new int[] { xcoor - i, ycoor - j });
                        }
                    }
                    j--;
                }
            }
        }

        public void bishopMoves(int xcoor, int ycoor, char c) {
            if (c == 'b')
            {
                int j = 1;
                for (int i = xcoor + 1; i < 8; i++) {
                    if ((ycoor + j) < 8) {
                        if (board[i, ycoor + j] > 10)
                        {
                            suggestedCapture.Add(new int[] { i, ycoor + j });
                            break;
                        }
                        else if (board[i, ycoor + j] == 0)
                            suggestedMoves.Add(new int[] { i, ycoor + j });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = xcoor + 1; i < 8; i++)
                {
                    if ((ycoor - j) >= 0)
                    {
                        if (board[i, ycoor - j] > 10)
                        {
                            suggestedCapture.Add(new int[] { i, ycoor - j });
                            break;
                        }
                        else if (board[i, ycoor - j] == 0)
                            suggestedMoves.Add(new int[] { i, ycoor - j });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = ycoor + 1; i < 8; i++)
                {
                    if ((xcoor - j) >= 0)
                    {
                        if (board[xcoor - j, i] > 10)
                        {
                            suggestedCapture.Add(new int[] { xcoor - j, i });
                            break;
                        }
                        else if (board[xcoor - j, i] == 0)
                            suggestedMoves.Add(new int[] { xcoor - j, i });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = ycoor-1; i >= 0; i--)
                {
                    if ((xcoor - j) >= 0)
                    {
                        if (board[xcoor - j, i] > 10)
                        {
                            suggestedCapture.Add(new int[] { xcoor - j, i });
                            break;
                        }
                        else if (board[xcoor - j, i] == 0)
                            suggestedMoves.Add(new int[] { xcoor - j, i });
                        else
                            break;
                        j++;
                    }
                }
            }

            if (c == 'w')
            {
                int j = 1;
                for (int i = xcoor + 1; i < 8; i++)
                {
                    if ((ycoor + j) < 8)
                    {
                        if (board[i, ycoor + j] <= 10 && board[i, ycoor + j] > 0)
                        {
                            suggestedCapture.Add(new int[] { i, ycoor + j });
                            break;
                        }
                        else if (board[i, ycoor + j] == 0)
                            suggestedMoves.Add(new int[] { i, ycoor + j });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = xcoor + 1; i < 8; i++)
                {
                    if ((ycoor - j) >= 0)
                    {
                        if (board[i, ycoor - j] <= 10 && board[i, ycoor - j] > 0)
                        {
                            suggestedCapture.Add(new int[] { i, ycoor - j });
                            break;
                        }
                        else if (board[i, ycoor - j] == 0)
                            suggestedMoves.Add(new int[] { i, ycoor - j });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = ycoor + 1; i < 8; i++)
                {
                    if ((xcoor - j) >= 0)
                    {
                        if (board[xcoor - j, i] <= 10 && board[xcoor - j, i] > 0)
                        {
                            suggestedCapture.Add(new int[] { xcoor - j, i });
                            break;
                        }
                        else if (board[xcoor - j, i] == 0)
                            suggestedMoves.Add(new int[] { xcoor - j, i });
                        else
                            break;
                        j++;
                    }
                }

                j = 1;

                for (int i = ycoor - 1; i >= 0; i--)
                {
                    if ((xcoor - j) >= 0)
                    {
                        if (board[xcoor - j, i] <= 10 && board[xcoor - j, i] > 0)
                        {
                            suggestedCapture.Add(new int[] { xcoor - j, i });
                            break;
                        }
                        else if (board[xcoor - j, i] == 0)
                            suggestedMoves.Add(new int[] { xcoor - j, i });
                        else
                            break;
                        j++;
                    }
                }
            }
        }

        public void rookMoves(int xcoor, int ycoor, char c) {
            if (c == 'b')
            {
                for (int i = xcoor + 1; i <= 7; i++) {
                    if (board[i, ycoor] > 10) {
                        suggestedCapture.Add(new int[] { i, ycoor });
                        break;
                    }
                    else if (board[i, ycoor] == 0)
                        suggestedMoves.Add(new int[] { i, ycoor });
                    else
                        break;
                }

                for (int i = xcoor - 1; i >= 0; i--)
                {
                    if (board[i, ycoor] > 10) {
                        suggestedCapture.Add(new int[] { i, ycoor });
                        break;
                    }
                    else if (board[i, ycoor] == 0)
                        suggestedMoves.Add(new int[] { i, ycoor });
                    else
                        break;
                }

                for (int i = ycoor + 1; i <= 7; i++)
                {
                    if (board[xcoor, i] > 10)
                    {
                        suggestedCapture.Add(new int[] { xcoor, i });
                        break;
                    }
                    else if (board[xcoor, i] == 0)
                        suggestedMoves.Add(new int[] { xcoor, i });
                    else
                        break;
                }

                for (int i = ycoor - 1; i >= 0; i--)
                {
                    if (board[xcoor, i] > 10)
                    {
                        suggestedCapture.Add(new int[] { xcoor, i });
                        break;
                    }
                    else if (board[xcoor, i] == 0)
                        suggestedMoves.Add(new int[] { xcoor, i });
                    else
                        break;
                }
            }

            if (c == 'w')
            {
                for (int i = xcoor + 1; i <= 7; i++)
                {
                    if (board[i, ycoor] <= 10 && board[i, ycoor] > 0)
                    {
                        suggestedCapture.Add(new int[] { i, ycoor });
                        break;
                    }
                    else if (board[i, ycoor] == 0)
                        suggestedMoves.Add(new int[] { i, ycoor });
                    else
                        break;
                }

                for (int i = xcoor - 1; i >=0 ; i--)
                {
                    if (board[i, ycoor] <= 10 && board[i, ycoor] > 0)
                    {
                        suggestedCapture.Add(new int[] { i, ycoor });
                        break;
                    }
                    else if (board[i, ycoor] == 0)
                        suggestedMoves.Add(new int[] { i, ycoor });
                    else
                        break;
                }

                for (int i = ycoor + 1; i <= 7; i++)
                {
                    if (board[xcoor, i] <= 10 && board[xcoor, i] > 0)
                    {
                        suggestedCapture.Add(new int[] { xcoor, i });
                        break;
                    }
                    else if (board[xcoor, i] == 0)
                        suggestedMoves.Add(new int[] { xcoor, i });
                    else
                        break;
                }

                for (int i = ycoor - 1; i >= 0; i--)
                {
                    if (board[xcoor, i] <= 10 && board[xcoor, i] > 0)
                    {
                        suggestedCapture.Add(new int[] { xcoor, i });
                        break;
                    }
                    else if (board[xcoor, i] == 0)
                        suggestedMoves.Add(new int[] { xcoor, i });
                    else
                        break;
                }
            }
        }

        public void pawnMoves(int xcoor, int ycoor, char c) {
            if (c == 'b')
            {
                if (xcoor == 1)
                    suggestedMoves.Add(new int[] { (xcoor + 2), ycoor });

                if (!((ycoor - 1) == -1))
                    if (board[xcoor + 1, ycoor - 1] > 10)
                        suggestedCapture.Add(new int[] { (xcoor + 1), (ycoor - 1) });


                if (!((ycoor + 1) == 8))
                    if (board[xcoor + 1, ycoor + 1] > 10)
                        suggestedCapture.Add(new int[] { (xcoor + 1), (ycoor + 1) });

                if (!(board[xcoor + 1, ycoor] > 0))
                    suggestedMoves.Add(new int[] { (xcoor + 1), ycoor });
            }
            else if (c == 'w')
            {
                if (xcoor == 6)
                    suggestedMoves.Add(new int[] { (xcoor - 2), ycoor });

                if (!((ycoor - 1) == -1))
                    if (board[xcoor - 1, ycoor - 1] <= 10 && board[xcoor - 1, ycoor - 1] > 0)
                        suggestedCapture.Add(new int[] { (xcoor - 1), (ycoor - 1) });

                if (!((ycoor + 1) == 8))
                    if (board[xcoor - 1, ycoor + 1] <= 10 && board[xcoor - 1, ycoor + 1] > 0)
                        suggestedCapture.Add(new int[] { (xcoor - 1), (ycoor + 1) });

                if (!(board[xcoor - 1, ycoor] > 0))
                    suggestedMoves.Add(new int[] { (xcoor - 1), ycoor });
            }
        }
    }
}
